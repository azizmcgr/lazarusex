unit formMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, StdCtrls, fpjson, jsonparser,
  DB, BufDataset, memds, Grids, DBGrids, uRESTRequest;

type

  { TForm2 }

  TForm2 = class(TForm)
    btnPut: TButton;
    btnGet: TButton;
    btnPost: TButton;
    btnDelete: TButton;
    MemDataset1: TMemDataset;
    Memo1: TMemo;
    Label1: TLabel;
    Memo2: TMemo;
    Label2: TLabel;
    DBGrid1: TDBGrid;
    Label3: TLabel;
    DataSource1: TDataSource;
    btnIDHttpGetTest: TButton;
    btnIDHttpPostTeste: TButton;
    procedure btnPutClick(Sender: TObject);
    procedure btnGetClick(Sender: TObject);
    procedure btnPostClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnIDHttpGetTestClick(Sender: TObject);
    procedure btnIDHttpPostTesteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    RESTClient : TRESTClient;
    procedure ListAlunos(Value : String);
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

{$IFDEF LCL}
{$R *.lfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

procedure TForm2.ListAlunos(Value : String);
Var
 s, lResponse : String;
 LJsonObject  : TJSONData;
 Parser       : TJSONParser;
 jsPair       : TJSONData;
 Arr          : TJSONArray;
 I            : Integer;
begin
 Try
  Try
   lResponse := RESTClient.SendEvent('GetListaAlunos/' + Value);
  Except
   Exit;
  End;
  Memo1.Lines.Clear;
  Memo1.Lines.add(lResponse);
 Finally
  If lResponse <> '' Then
   Begin
    Parser      := TJSONParser.Create(lResponse);
    LJsonObject := Parser.Parse As TJSONData;
    If LJsonObject <> Nil Then
     Begin
      If LJsonObject.Count > 0 Then
       Begin
        MemDataset1.DisableControls;
        MemDataset1.Close;
        MemDataset1.CreateTable;
        MemDataset1.Open;
        Arr   := LJsonObject.Items[0] As TJSONArray;
        For I := 0 To Arr.Count -1 Do
         Begin
          MemDataset1.Insert;
          jsPair    := Arr.Objects[I];
          s := jsPair.GetPath('NomeAluno').AsString;
          MemDataset1.FieldByName('Alunos').AsString := s;
          MemDataset1.Post;
         End;
        MemDataset1.EnableControls;
       End;
     End;
   End;
  LJsonObject.Free;
  MemDataset1.First;
 end;
end;


procedure TForm2.btnGetClick(Sender: TObject);
Var
 lResponse,
 Aluno : String;
Begin
 Aluno := InputBox('Rest Client', 'Nome do aluno', '');
 If Aluno <> '' Then
  Begin
   Try
    lResponse := RESTClient.SendEvent('ConsultaAluno/' + Aluno);
    Memo2.Lines.Clear;
    Memo2.Lines.add(lResponse);
    ListAlunos(Aluno);
   Except
   End;
  End;
End;

procedure TForm2.btnPostClick(Sender: TObject);
Var
 lResponse,
 Aluno : String;
 NomeNovo : string;
 RBody: TStringList;
Begin
 RBody := TStringList.Create;
 RBody.Add('json');
 Aluno := InputBox('Rest Client', 'Nome do aluno', '');
 If Aluno <> '' Then
  Begin
   NomeNovo := InputBox('Rest Client', 'Alterar Nome para', '');
   If NomeNovo <> '' Then
    Begin
     Try
      lResponse := RESTClient.SendEvent('AtualizaAluno/' + Aluno + '/' + NomeNovo, RBody, sePost);
      ListAlunos(lResponse);
     Except
     End;
    End;
  End;
 RBody.Free;
End;

procedure TForm2.btnPutClick(Sender: TObject);
Var
 lResponse,
 Aluno : String;
 RBody : TStringList;
Begin
 RBody := TStringList.Create;
 RBody.Add('json');
 Aluno := InputBox('Rest Client', 'Nome do aluno', '');
 If Aluno <> '' Then
  Begin
   Try
    lResponse := RESTClient.SendEvent('InsereAluno/' + Aluno, RBody, sePut);
    ListAlunos(lResponse);
   Except
   End;
  End;
 RBody.Free;
End;


procedure TForm2.btnDeleteClick(Sender: TObject);
Var
 lResponse,
 Aluno : String;
Begin
 Aluno := InputBox('Rest Client', 'Nome do aluno', '');
 If Aluno <> '' Then
  Begin
   Try
    lResponse := RESTClient.SendEvent('ExcluiAluno/' + Aluno, Nil, seDelete);
    ListAlunos(lResponse);
   Except
   End;
  End;
End;

procedure TForm2.btnIDHttpGetTestClick(Sender: TObject);
Var
 Response : String;
Begin
 Memo2.Lines.Clear;
 Try
  // Passando par�metros no formato antigo (QueryString)
  Response := RESTClient.SendEvent('ConsultaAluno?Nome=AlunoTeste');
  Memo2.Lines.Add(Response);
  // Passando par�metros no formato novo (REST URL)
  Response := RESTClient.SendEvent('ConsultaAluno/AlunoTeste');
  Memo2.Lines.Add(Response);
  ListAlunos(Response);
 Finally
 End;
End;

procedure TForm2.btnIDHttpPostTesteClick(Sender: TObject);
Var
 Response : String;
 lParams  : TStringList;
Begin
 Memo2.Lines.Clear;
 lParams := TStringList.Create;
 Try
  //Aqui o par�metro � passado no header da requisi��o e n�o na URL
  //da mesma forma que todos os navegadores o fazem
  //ou seja � possivel que voce tenha um client em HTML puro
  //dando POST no navegador num WebService em Lazarus
  lParams.Add('NomeAtual=Fulano');
  lParams.Add('NomeNovo=Cicrano');
  Response := RESTClient.SendEvent('AtualizaAluno', lParams, sePOST);
  Memo2.Lines.Add(Response);
  ListAlunos(Response);
 Finally
  lParams.Free;
 End;
End;

procedure TForm2.FormCreate(Sender: TObject);
Begin
 RESTClient      := TRESTClient.Create(Self);
 RESTClient.Host := 'localhost';
 RESTClient.Port := 8080;
End;

end.
