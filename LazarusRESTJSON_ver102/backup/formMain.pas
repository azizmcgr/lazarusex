(*
   ------------------------------------
   RestWebService - Ver�o 1.0

   Este Web service Application foi desenvolvido para possibilitar
   a exist�ncia de WebServices REST+JSON em Delphi na vers�o PROFESSIONAL
   a partir da XE2 na qual esta aplica��o foi desenvolvida :
     - sem a utiliza��o de Datasnap,
     - Sem necessidade de utiliza��o da Vers�o Enterprise do Delphi
     - Sem outros frameworks externos (free ou n�o)
   mas apenas utilizando o INDY que vem com o Delphi em todas as
   edi��es.

   Foi feito e testado na XE2 professional, mas � muito prov�vel
   que funcionar� em todas as vers�es posteriores at� o momento
   at� o momento a �ltima vers�o do Delphi � (ou �ra) a DX Seattle

   -----------------------------------
   Autor : Claudio Ferreira

   Licen�a :
     Livre para copiar, usar e melhorar, pe�o apenas que mantenha
     este cabe�alho

   Novas vers�es melhoradas acrescente aqui as melhorias e autoria

     ** Vers�o 1.11 - 08/11/2016 - Claudio Ferreira

        1) Agora um client do tipo navegador pode enviar um HTTP POST
        ou HTTP GET com par�metros que o Servidor receber� e processar�
        normalmente no server Method. Ou Seja agora Podemos ter Clients
        Em Delphi, HTML (navegadores) ou outras linguagens.

        2) Agora retorna um erro em JSON se caso o Server Method informado
        N�o existir

     ** Vers�o 1.1 - Claudio Ferreira
        Foi Colocado Autentica��o (opcional) no servidor

     ** Vers�o 1.0 - Claudio Ferreira
        1a. Vers�o
*)

unit formMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Controls, Forms, Dialogs, StdCtrls, ExtCtrls, IdContext,
  Buttons, ServerUtils, IdHTTPServer, IdCustomHTTPServer;

type

  { TfrmMain }

  TfrmMain = class(TForm)
    //apl1: TApplicationEvents;
    IdHTTPServer1: TIdHTTPServer;
    TrayIcon1: TTrayIcon;
    txtInfoLabel: TStaticText;
    lblNome: TLabel;
    btnAtivar: TBitBtn;
    btnParar: TBitBtn;
    memoReq: TMemo;
    memoResp: TMemo;
    Label1: TLabel;
    Label2: TLabel;
    procedure TrayIcon1Click(Sender: TObject);
    procedure apl1Minimize(Sender: TObject);
    procedure btnAtivarClick(Sender: TObject);
    procedure btnPararClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure IdHTTPServer1CommandGet(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure IdHTTPServer1CommandOther(AContext: TIdContext;
      ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    ServerParams : TServerParams;
    procedure LoglastRequest (ARequestInfo: TIdHTTPRequestInfo);
    procedure LogLastResponse (AResponseInfo: TIdHTTPResponseInfo);
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;
  CriticalSection: TRTLCriticalSection;

implementation

{$IFDEF LCL}
{$R *.lfm}
{$ELSE}
{$R *.dfm}
{$ENDIF}

Uses
     Systypes, ServerMethodsUnit1;


procedure TfrmMain.FormCreate(Sender: TObject);
begin
     btnAtivarClick(self);
     ServerParams := TServerParams.Create;
     //ServerParams.HasAuthentication := True;
     ServerParams.UserName          := 'user';
     ServerParams.Password          := 'passwd';
end;


procedure TfrmMain.FormDestroy(Sender: TObject);
begin
     ServerParams.Free;
end;

procedure TfrmMain.TrayIcon1Click(Sender: TObject);
begin
     TrayIcon1.Visible := False;
     Show();
     WindowState := wsNormal;
     Application.BringToFront();
end;

procedure TfrmMain.apl1Minimize(Sender: TObject);
begin
     Self.Hide();
     Self.WindowState := wsMinimized;
     TrayIcon1.Visible := True;
     TrayIcon1.Animate := True;
     TrayIcon1.ShowBalloonHint;
end;

procedure TfrmMain.btnAtivarClick(Sender: TObject);
begin
     IdHTTPServer1.Active := True;
     txtInfoLabel.Caption := 'Aguardando requisi��es...';
end;


procedure TfrmMain.btnPararClick(Sender: TObject);
begin
     IdHTTPServer1.Active := False;
     txtInfoLabel.Caption := 'WebService parado.';
end;

procedure TfrmMain.LoglastRequest (ARequestInfo: TIdHTTPRequestInfo);
Begin
     EnterCriticalSection(CriticalSection);
     memoReq.Lines.Add(ARequestInfo.UserAgent + #13 + #10 +
                       ARequestInfo.RawHTTPCommand);
     LeaveCriticalSection(CriticalSection);
End;

procedure TfrmMain.LogLastResponse (AResponseInfo: TIdHTTPResponseInfo);
Begin
     EnterCriticalSection(CriticalSection);
     memoResp.Lines.Add(AResponseInfo.ContentText);
     LeaveCriticalSection(CriticalSection);
End;


procedure TfrmMain.IdHTTPServer1CommandGet(AContext: TIdContext;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
Var
     Cmd           : String;
     Argumentos    : TArguments;
     ServerMethod1 : TServerMethods1;
     JSONStr       : string;
begin
     Cmd := ARequestInfo.RawHTTPCommand;

     If (ServerParams.HasAuthentication) then Begin
        if Not ((ARequestInfo.AuthUsername = ServerParams.Username) and
               (ARequestInfo.AuthPassword = ServerParams.Password))
         Then Begin
           AResponseInfo.AuthRealm := 'Forne�a autentica��o';
           AResponseInfo.WriteContent;
           Exit;
         End;
     End;
     if (UpperCase(Copy (Cmd, 1, 3)) = 'GET') OR
        (UpperCase(Copy (Cmd, 1, 4)) = 'POST') OR
        (UpperCase(Copy (Cmd, 1, 3)) = 'HEAD')
     then Begin
        if ARequestInfo.URI <> '/favicon.ico' Then Begin
           if ARequestInfo.Params.Count > 0 then begin
              Argumentos := TServerUtils.ParseWebFormsParams (ARequestInfo.Params, ARequestInfo.URI);
           End Else
              Argumentos := TServerUtils.ParseRESTURL (ARequestInfo.URI);

           ServerMethod1 := TServerMethods1.Create (nil);
           Try
              LoglastRequest (ARequestInfo);
              If UpperCase(Copy (Cmd, 1, 3)) = 'GET' Then
                 JSONStr := ServerMethod1.CallGETServerMethod(Argumentos);
              If UpperCase(Copy (Cmd, 1, 4)) = 'POST' Then
                 JSONStr := ServerMethod1.CallPOSTServerMethod(Argumentos);

              AResponseInfo.ContentText := JSONStr;
              LoglastResponse (AResponseInfo);
              AResponseInfo.WriteContent;
           Finally
              ServerMethod1.Free;
           End;
        End;
     end;
end;

// TRATE AQUI PUT E DELETE
procedure TfrmMain.IdHTTPServer1CommandOther(AContext: TIdContext;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
Var
     Cmd           : String;
     Argumentos    : TArguments;
     ServerMethod1 : TServerMethods1;
     JSONStr       : string;
begin
     Cmd := ARequestInfo.RawHTTPCommand;
     If (ServerParams.HasAuthentication) then Begin
        if Not ((ARequestInfo.AuthUsername = ServerParams.Username) and
               (ARequestInfo.AuthPassword = ServerParams.Password))
         Then Begin
           AResponseInfo.AuthRealm := 'Forne�a autentica��o';
           AResponseInfo.WriteContent;
           Exit;
         End;
     End;
     if (UpperCase(Copy (Cmd, 1, 3)) = 'PUT') OR
        (UpperCase(Copy (Cmd, 1, 6)) = 'DELETE')
     then Begin
        Argumentos    := TServerUtils.ParseRESTURL (ARequestInfo.URI);
        ServerMethod1 := TServerMethods1.Create (nil);
        Try
           LoglastRequest (ARequestInfo);
           If UpperCase(Copy (Cmd, 1, 3)) = 'PUT' Then
              JSONStr := ServerMethod1.CallPUTServerMethod(Argumentos);
           If UpperCase(Copy (Cmd, 1, 6)) = 'DELETE' Then
              JSONStr := ServerMethod1.CallDELETEServerMethod(Argumentos);

           AResponseInfo.ContentText := JSONStr;
           LoglastResponse (AResponseInfo);
           AResponseInfo.WriteContent;
        Finally
           ServerMethod1.Free;
        End;
     end;
end;

initialization
  InitializeCriticalSection(CriticalSection);

finalization
  DeleteCriticalSection(CriticalSection);


end.
