unit frm1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls
  , fphttpclient
  , fpjson, jsonparser;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    ListBox1: TListBox;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
  private
    function FPHTTPClientDownload(URL: string; SaveToFile: boolean=false;
      Filename: string=''): string;

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  listitemsjson:string;
  J: TJSONData;
  c: integer;
begin
  listitemsjson := FPHTTPClientDownload('https://jsonplaceholder.typicode.com/todos');
  J := GetJSON(listitemsjson);

  for c := 0 to J.Count - 1 do
  begin
    try
      ListBox1.Items.Add( J.Items[c].FindPath('title').AsString );
    finally
    end;
  end;
end;

procedure TForm1.ListBox1Click(Sender: TObject);
var
  listitemsjson:string;
  J: TJSONData;
begin
  listitemsjson := FPHTTPClientDownload('https://jsonplaceholder.typicode.com/todos/'+IntToStr(ListBox1.ItemIndex+1));
  J := GetJSON(listitemsjson);
  Memo1.Clear;
  Memo1.Lines.Add( 'Title: ' + J.FindPath('title').AsString );
  Memo1.Lines.Add( 'Completed: ' + J.FindPath('completed').AsString );
end;

function TForm1.FPHTTPClientDownload(URL: string; SaveToFile: boolean = false; Filename: string = ''): string;
begin
  // Result will be:
  // - empty ('') when it has failed
  // - filename when the file has been downloaded successfully
  // - content when the content SaveToFile is set to False
  Result := '';
  With TFPHttpClient.Create(Nil) do
  try
    try
      if SaveToFile then begin
        Get(URL, Filename);
        Result := Filename;
      end else begin
        Result := Get(URL);
      end;
    except
      on E: Exception do
        ShowMessage('Error: ' + E.Message);
    end;
  finally
    Free;
  end;
end;

end.

