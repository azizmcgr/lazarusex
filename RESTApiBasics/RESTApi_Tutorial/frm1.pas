// Downloaded from: https://lazplanet.blogspot.com

unit frm1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls,
  fphttpclient, fpjson, jsonparser;

type

  { TForm1 }

  TForm1 = class(TForm)
    Button1: TButton;
    Edit1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    ListBox1: TListBox;
    Memo1: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
  private
    function FPHTTPClientDownload(URL: string; SaveToFile: boolean=false;
      Filename: string=''): string;

  public

  end;

var
  Form1: TForm1;
  woeids: TStringList;

implementation

{$R *.lfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  listitemsjson:string;
  J: TJSONData;
  c: integer;
begin
  listitemsjson := FPHTTPClientDownload('https://www.metaweather.com/api/location/search/?query='+Edit1.Text);
  J := GetJSON(listitemsjson);

  ListBox1.Clear;
  woeids.Clear;

  for c := 0 to J.Count - 1 do
  begin
    try
      ListBox1.Items.Add( J.Items[c].FindPath('title').AsString );
      // we keep the woeid in a "virtual list" so that we can use it later
      woeids.Add( J.Items[c].FindPath('woeid').AsString );
    finally
    end;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  woeids := TStringList.Create;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  woeids.Free;
end;

procedure TForm1.ListBox1Click(Sender: TObject);
var
  listitemsjson:string;
  J: TJSONData;
  YY,MM,DD : Word;
  datestring: string;
begin
  // prepare the date string for use on the URL
  DeCodeDate (Date,YY,MM,DD);
  datestring:=format ('%d/%d/%d',[yy,mm,dd]);

  // get JSON response
  listitemsjson := FPHTTPClientDownload(
                       'https://www.metaweather.com/api/location/'
                       +woeids.Strings[ListBox1.ItemIndex]
                       +'/'+datestring+'/');
  J := GetJSON(listitemsjson);

  // show the data
  Memo1.Clear;
  with J.Items[0] do begin
    Memo1.Lines.Add( 'Weather state: ' + FindPath('weather_state_name').AsString );
    Memo1.Lines.Add( 'Minimum Temp: ' + FloatToStrF( FindPath('min_temp').AsFloat, ffFixed, 8, 2 ) + '°C' );
    Memo1.Lines.Add( 'Maximum Temp: ' + FloatToStrF( FindPath('max_temp').AsFloat, ffFixed, 8, 2 ) + '°C' );
    Memo1.Lines.Add( 'Temp: ' + FloatToStrF( FindPath('the_temp').AsFloat, ffFixed, 8, 2 ) + '°C' );
    Memo1.Lines.Add( 'Wind speed: ' + FloatToStrF( FindPath('wind_speed').AsFloat, ffFixed, 8, 2 ) );
    Memo1.Lines.Add( 'Air pressure: ' + FloatToStrF( FindPath('air_pressure').AsFloat, ffFixed, 8, 2 ) );
  end;
end;

function TForm1.FPHTTPClientDownload(URL: string; SaveToFile: boolean = false; Filename: string = ''): string;
begin
  Result := '';
  With TFPHttpClient.Create(Nil) do
  try
    try
      if SaveToFile then begin
        Get(URL, Filename);
        Result := Filename;
      end else begin
        Result := Get(URL);
      end;
    except
      on E: Exception do
        ShowMessage('Error: ' + E.Message);
    end;
  finally
    Free;
  end;
end;

end.

